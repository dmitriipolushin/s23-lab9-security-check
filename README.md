# Lab9 -- Security check

Tested site is [vc.ru](https://vc.ru/)

## Forgot Password

### Existing account case

| Test step | Result |
| - | - |
| Visit https://vc.ru/ | OK |
|Press on "Forgot password"| Still on main page https://vc.ru/popular |
|Enter email for existing account and click "Send"|OK|
|Check response (using devtools) time and message|Response time - 817 ms, message - "We have sent an email with instructions for changing your password"|

### Nonexistent account case

| Test step | Result |
| - | - |
| Visit https://vc.ru/ | OK |
|Press on "Forgot password"| Still on main page https://vc.ru/popular |
Enter email for nonexistent account and click "Send"|OK|
|Check response (using devtools) time and message|Response time - 250 ms, message - NO ERROR MESSAGE|

### OWASP requirements summary

| Requirement | Result |
| - | - |
| Return a consistent message for both existent and non-existent accounts | - |
| Ensure that the time taken for the user response message is uniform | - |
| Use a side-channel to communicate the method to reset their password | + |

## Case-insensitive User ID

my login is 27102001@bk.ru

### Authorization case

| Test step | Result |
| - | - |
| Visit https://vc.ru/ | OK |
| Enter a login 27102001@BK.RU and correct password | OK|
| Check that authorization is successful | OK |

### Creating Account case
 Test step | Result |
| - | - |
| Visit https://vc.ru/ | OK |
| Enter a login 27102001@bk.ru and other information to register new account | OK|
| Check that website disallows repeating emails  |Get error message - "The user with this email is already registered" |

### OWASP requirements summary

| Requirement | Result |
| - | - |
|Usernames/user IDs should be case-insensitive| + |
|Usernames should also be unique|+|